﻿using Microsoft.AspNetCore.Mvc;
using PretestCoreNisrina.Models;
using PretestCoreNisrina.Repository;
using Microsoft.AspNetCore.Authorization;
using Dapper;
using System.Data;

namespace PretestCoreNisrina.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("DocumentList")]
        //[Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelDocument>();

            return Ok(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        //[Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idCompany", document.IDCompany, DbType.Int32);
            dp_param.Add("idCategory", document.IDCategory, DbType.Int32);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.String);
            dp_param.Add("iduser", document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelCompany>("sp_createDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idCompany", document.IDCompany, DbType.Int32);
            dp_param.Add("idCategory", document.IDCategory, DbType.Int32);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.String);
            dp_param.Add("iduser", document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);
            var result = _authentication.Execute_Command<ModelDocument>("sp_updateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        //[Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_deleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}
