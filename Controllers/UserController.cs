﻿using Microsoft.AspNetCore.Mvc;
using PretestCoreNisrina.Repository;
using Microsoft.AspNetCore.Authorization;
using Dapper;
using PretestCoreNisrina.Models;
using System.Data;

namespace PretestCoreNisrina.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public UserController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }
        [HttpPost("Login")]
        [AllowAnonymous]
        public IActionResult Login([System.Web.Http.FromBody] LoginModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Paramter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("email", user.Email, DbType.String);
            dp_param.Add("password", user.Password, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_loginUser", dp_param);

            if (result.Code == 200)
            {
                var token = _authentication.GenerateJWT(result.Data);

                return Ok(token);
            }

            return NotFound(result.Data);
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("UserList")]
        //[Authorize]
        public IActionResult getUser()
        {
            var result = _authentication.getUserList<ModelUser>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelUser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idCompany", user.IDCompany, DbType.Int32);
            dp_param.Add("idPosition", user.IDPosition, DbType.Int32);
            dp_param.Add("name", user.Name);
            dp_param.Add("address", user.Address);
            dp_param.Add("telephone", user.Telephone);
            dp_param.Add("email", user.Email);
            dp_param.Add("username", user.Username);
            dp_param.Add("password", user.Password);
            dp_param.Add("role", user.Role);
            dp_param.Add("flag", user.Flag, DbType.Int32);
            dp_param.Add("createdBy", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);


            var result = _authentication.Execute_Command<ModelUser>("sp_createUser", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelUser user, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idCompany", user.IDCompany, DbType.Int32);
            dp_param.Add("idPosition", user.IDPosition, DbType.Int32);
            dp_param.Add("name", user.Name);
            dp_param.Add("address", user.Address);
            dp_param.Add("telephone", user.Telephone);
            dp_param.Add("email", user.Email);
            dp_param.Add("username", user.Username);
            dp_param.Add("password", user.Password);
            dp_param.Add("role", user.Role);
            dp_param.Add("flag", user.Flag, DbType.Int32);
            dp_param.Add("createdBy", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_updateUser", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_deleteUser", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }




}
