﻿using Dapper;
using PretestCoreNisrina.Models;
using PretestCoreNisrina.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;


namespace PretestCoreNisrina.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentCategoryController : ControllerBase
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentCategoryController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("DocumentCategoryList")]

        //[Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult getDocumentCategory()
        {
            var result = _authentication.getDocumentCategoryList<ModelDocumentCategory>();

            return Ok(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        //[Authorize(Roles = "Admin")]
        [AllowAnonymous]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocumentCategory DocumentCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", DocumentCategory.Name, DbType.String);
            dp_param.Add("iduser", DocumentCategory.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_createDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocumentCategory DocumentCategory, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", DocumentCategory.Name, DbType.String);
            dp_param.Add("iduser", DocumentCategory.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_updateDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_deleteDocumentCategory", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}