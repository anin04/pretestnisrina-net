﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using PretestCoreNisrina.Models;
using PretestCoreNisrina.Repository;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace PretestCoreNisrina.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public PositionController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }
        [Microsoft.AspNetCore.Mvc.HttpGet("PositionList")]
        //[Authorize]
        public IActionResult getPosition()
        {
            var result = _authentication.getPositionList<ModelPosition>();

            return Ok(result);
        }
        [HttpPost("Create")]
        [AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelPosition position)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("iduser", position.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_createPosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelPosition position, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("id", id, DbType.String);
            dp_param.Add("name", position.Name);
            dp_param.Add("iduser", position.CreatedBy);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);


            var result = _authentication.Execute_Command<ModelPosition>("sp_updatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_deletePosition", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}

