﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreNisrina.Models
{
    public class ModelUser
    {[Required]
        public int? IDCompany { get; set; }
        [Required]
        public int? IDPosition { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Address { get; set; }
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Telephone { get; set; }
      
        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public string? Role { get; set; }
        [Required]
        public int? Flag { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;

    }
}
