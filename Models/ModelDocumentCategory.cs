﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreNisrina.Models
{
    public class ModelDocumentCategory
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
        [Required]
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
